import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import BusLinesPage from '@/pages/BusLinesPage.vue';
import BusStopsPage from '@/pages/BusStopsPage.vue';

const routes: Array<RouteRecordRaw> = [
    {
        path: '',
        redirect: { name: 'busLines' }
    },
    {
        path: '/bus-lines',
        name: 'busLines',
        component: BusLinesPage
    },
    {
        path: '/stops',
        name: 'stops',
        component: BusStopsPage
    }
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
});

export default router;
