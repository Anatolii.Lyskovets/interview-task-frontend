import { Module } from 'vuex';
import { BusStopsClientInstance } from '@/service/ApiClients/BusStopsClient';
import { BusStopContract } from '@/service/Contracts/BusStopContract';

export interface BusStopsModuleState {
    busStops: Array<BusStopContract>;
    groupedStops: Map<number, Map<string, Array<BusStopContract>>>;
}
export const BusStopsModule: Module<BusStopsModuleState, unknown> = {
    state: (): BusStopsModuleState => ({
        busStops: [],
        groupedStops: new Map
    }),
    getters: {
        busLines (state) {
            return Array.from(new Set(state.busStops.map(stop => stop.line))).sort();
        },
        getAllStopsNames (state) {
            return Array.from(new Set(state.busStops.map((busStop) => busStop.stop)));
        },
        getStopsByLine: (state) => (line: number) => {
            return state.groupedStops.get(line);
        },
        getStopsByLineAndStopName: (state) => (line: number, stop: string) => {
            return state.groupedStops.get(line)?.get(stop);
        }
    },
    mutations: {
        setBusStops (state, busStops: BusStopContract[]) {
            state.busStops = busStops;
        },
        populateGroupedStops (state, busStops: BusStopContract[]) {
            for (const busStop of busStops) {
                if (!state.groupedStops.has(busStop.line)) {
                    state.groupedStops.set(busStop.line, new Map);
                }
                if (!state.groupedStops.get(busStop.line)?.has(busStop.stop)) {
                    state.groupedStops.get(busStop.line)?.set(busStop.stop, []);
                }
                state.groupedStops.get(busStop.line)?.get(busStop.stop)?.push(busStop);
            }
        }
    },
    actions: {
        async loadBusStops (context, { fresh = false } = {}) {
            if (context.state.busStops.length === 0 || fresh) {
                const response = await BusStopsClientInstance.getBusStops();
                context.commit('setBusStops', response.data);
                context.commit('populateGroupedStops', response.data);
            }
        },
    }
};
