import { createStore } from 'vuex';
import { BusStopsModule } from '@/store/modules/BusStopsModule';

export default createStore({
    state: {},
    getters: {},
    mutations: {},
    actions: {},
    modules: {
        BusStopsModule
    }
});
