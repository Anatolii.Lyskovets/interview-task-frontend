export interface BusStopContract {
    line: number
    stop: string
    order: number
    time: string
}
