import globalAxios, { AxiosInstance } from 'axios';
import { BusStopContract } from '@/service/Contracts/BusStopContract';
import axios from 'axios';

export class BusStopsClient {
    private _axios: AxiosInstance

    constructor(axios: AxiosInstance = globalAxios) {
        this._axios = axios;
    }

    async getBusStops() {
        return await this._axios.get<BusStopContract[]>('/stops');
    }
}

export const BusStopsClientInstance = new BusStopsClient(
    axios.create({ baseURL: process.env.VUE_APP_API_BASE_URL })
);
